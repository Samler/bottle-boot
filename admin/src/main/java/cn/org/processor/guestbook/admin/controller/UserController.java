package cn.org.processor.guestbook.admin.controller;

import cn.org.processor.guestbook.admin.model.request.UserChangePasswordRequest;
import cn.org.processor.guestbook.admin.model.request.UserChangeRoleRequest;
import cn.org.processor.guestbook.admin.service.UserService;
import entity.UserProfile;
import io.Page;
import io.Response;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@RestController
@RequestMapping("user")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * 获取用户详细信息
     *
     * @param id user id
     * @return {@link Response}
     */
    @GetMapping("{id}")
    public Response detail(@PathVariable @Validated
                               @NotNull(message = "参数错误")
                               @Positive(message = "用户id有误") Integer id) {
        return userService.detail(id);
    }

    /**
     * 列出所有用户
     *
     * @param page {@link Page}
     * @return {@link Response}
     */
    @PostMapping("list")
    public Response list(@RequestBody @Validated Page page) {
        return userService.list(page);
    }

    /**
     * 锁定用户
     *
     * @param id 用户ID
     * @return {@link Response}
     */
    @PostMapping("lock/{id}")
    public Response lock(@PathVariable @Validated
                             @NotNull(message = "参数错误")
                             @Positive(message = "用户id有误") Integer id) {
        return userService.lock(id);
    }

    /**
     * 解锁用户
     *
     * @param id 用户ID
     * @return {@link Response}
     */
    @PostMapping("unlock/{id}")
    public Response unlock(@PathVariable @Validated
                         @NotNull(message = "参数错误")
                         @Positive(message = "用户id有误") Integer id) {
        return userService.unlock(id);
    }

    /**
     * 修改用户的密码
     *
     * @param request {@link UserChangePasswordRequest}
     * @return {@link Response}
     */
    @PostMapping("password")
    public Response changePassword(@RequestBody @Validated UserChangePasswordRequest request) {
        return userService.changePassword(request);
    }

    /**
     * 更改用户角色
     *
     * @param request @{link UserChangeRoleRequest}
     * @return {@link Response}
     */
    @PostMapping("role")
    public Response changeRole(@RequestBody @Validated UserChangeRoleRequest request) {
        return userService.changeRole(request);
    }

    /**
     * 更改用户资料
     *
     * @param profile {@link UserProfile}
     * @return {@link Response}
     */
    @PostMapping("profile")
    public Response changeProfile(@RequestBody @Validated @NotNull(message = "参数错误") UserProfile profile) {
        return userService.changeProfile(profile);
    }
}
