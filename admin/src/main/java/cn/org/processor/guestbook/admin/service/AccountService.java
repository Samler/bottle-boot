package cn.org.processor.guestbook.admin.service;

import cn.org.processor.guestbook.admin.model.request.AdminChangePasswordRequest;
import cn.org.processor.guestbook.admin.model.request.AdminLoginRequest;
import io.Response;

public interface AccountService {
    Response login(AdminLoginRequest request);

    Response changePassword(AdminChangePasswordRequest request);
}
