package cn.org.processor.guestbook.admin.service.impl;

import cn.org.processor.guestbook.admin.mapper.*;
import cn.org.processor.guestbook.admin.service.CommonService;
import entity.User;
import io.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CommonServiceImpl implements CommonService {
    private final UserMapper userMapper;
    private final WalletMapper walletMapper;
    private final SettleMapper settleMapper;
    private final TopicMapper topicMapper;
    private final MessageMapper messageMapper;

    public CommonServiceImpl(UserMapper userMapper, WalletMapper walletMapper, SettleMapper settleMapper,
                             TopicMapper topicMapper, MessageMapper messageMapper) {
        this.userMapper = userMapper;
        this.walletMapper = walletMapper;
        this.settleMapper = settleMapper;
        this.topicMapper = topicMapper;
        this.messageMapper = messageMapper;
    }

    @Override
    public Response getUserCount() {
        return Response.builder()
                .ret(Response.Ret.SUCCESS)
                .msg("获取成功")
                .data(userMapper.getCountByRole(User.Role.USER))
                .build();
    }

    @Override
    public Response getConsulterCount() {
        return Response.builder()
                .ret(Response.Ret.SUCCESS)
                .msg("获取成功")
                .data(userMapper.getCountByRole(User.Role.COUNSELOR))
                .build();
    }

    @Override
    public Response getTopicTotal() {
        return Response.builder()
                .ret(Response.Ret.SUCCESS)
                .msg("获取成功")
                .data(topicMapper.getCount())
                .build();
    }

    @Override
    public Response getBottleTotal() {
        return Response.builder()
                .ret(Response.Ret.SUCCESS)
                .msg("获取成功")
                .data(messageMapper.countByBottle())
                .build();
    }

    @Override
    public Response getCoinTotal() {
        return Response.builder()
                .ret(Response.Ret.SUCCESS)
                .msg("获取成功")
                .data(walletMapper.getCoinTotal())
                .build();
    }

    @Override
    public Response getSettleTotal() {
        return Response.builder()
                .ret(Response.Ret.SUCCESS)
                .msg("获取成功")
                .data(settleMapper.getSettleTotal())
                .build();
    }
}
