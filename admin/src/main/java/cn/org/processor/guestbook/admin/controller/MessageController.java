package cn.org.processor.guestbook.admin.controller;

import cn.org.processor.guestbook.admin.service.MessageService;
import io.Page;
import io.Response;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@RestController
@RequestMapping("message")
public class MessageController {
    private final MessageService messageService;

    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    /**
     * 获取留言列表
     *
     * @param page {@link Page}
     * @return {@link Response}
     */
    @PostMapping("list")
    public Response list(@RequestBody @Validated Page page) {
        return messageService.list(page);
    }

    /**
     * 锁定留言
     *
     * @param id topic id
     * @return {@link Response}
     */
    @PostMapping("lock/{id}")
    public Response lock(@PathVariable @Validated
                         @NotNull(message = "参数错误")
                         @Positive(message = "留言id有误") Integer id) {
        return messageService.lock(id);
    }

    /**
     * 解锁留言
     *
     * @param id topic id
     * @return {@link Response}
     */
    @PostMapping("unlock/{id}")
    public Response unlock(@PathVariable @Validated
                           @NotNull(message = "参数错误")
                           @Positive(message = "留言id有误") Integer id) {
        return messageService.unlock(id);
    }
}
