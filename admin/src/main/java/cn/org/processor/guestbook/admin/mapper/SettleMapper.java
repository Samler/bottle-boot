package cn.org.processor.guestbook.admin.mapper;

import cn.org.processor.guestbook.admin.model.response.SettleTotal;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SettleMapper {
    SettleTotal getSettleTotal();
}
