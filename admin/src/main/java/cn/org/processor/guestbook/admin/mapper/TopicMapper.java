package cn.org.processor.guestbook.admin.mapper;

import entity.Topic;
import io.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface TopicMapper {
    int getCount();

    List<Topic> list(@Param("start") int start, @Param("offset") Integer offset);

    Topic findById(int id);

    int update(Topic topic);
}
