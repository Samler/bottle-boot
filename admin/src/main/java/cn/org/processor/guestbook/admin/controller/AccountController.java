package cn.org.processor.guestbook.admin.controller;

import annotation.IgnoreTokenCheck;
import cn.org.processor.guestbook.admin.model.request.AdminChangePasswordRequest;
import cn.org.processor.guestbook.admin.model.request.AdminLoginRequest;
import cn.org.processor.guestbook.admin.service.AccountService;
import io.Response;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("account")
public class AccountController {
    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * 管理员登录
     *
     * @param request {@link AdminLoginRequest}
     * @return {@link Response}
     */
    @PostMapping("login")
    @IgnoreTokenCheck
    public Response login(@RequestBody @Validated AdminLoginRequest request) {
        return accountService.login(request);
    }

    /**
     * 管理员修改密码
     *
     * @param request {@link AdminChangePasswordRequest}
     * @return {@link Response}
     */
    @PostMapping("password")
    public Response changePassword(@RequestBody @Validated AdminChangePasswordRequest request) {
        return accountService.changePassword(request);
    }

    /**
     * 获取登录状态
     * 如果已经登录，便可以正常返回成功信息，否则将被拦截器拦截
     *
     * @return {@link Response}
     */
    @GetMapping("login_status")
    public Response loginStatus() {
        return Response.builder().ret(Response.Ret.SUCCESS).msg("已登录").build();
    }
}
