package cn.org.processor.guestbook.admin.mapper;

import entity.UserProfile;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserProfileMapper {
    UserProfile findById(int id);

    int updateAll(UserProfile profile);
}
