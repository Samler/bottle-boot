package cn.org.processor.guestbook.admin.service;

import io.Page;
import io.Response;

public interface MessageService {
    Response list(Page page);

    Response lock(Integer id);

    Response unlock(Integer id);
}
