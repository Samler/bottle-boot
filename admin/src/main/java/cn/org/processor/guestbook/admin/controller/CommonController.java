package cn.org.processor.guestbook.admin.controller;

import cn.org.processor.guestbook.admin.service.CommonService;
import io.Response;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("common")
public class CommonController {
    private final CommonService commonService;

    public CommonController(CommonService commonService) {
        this.commonService = commonService;
    }

    /**
     * 获取普通用户总数
     *
     * @return {@link Response}
     */
    @GetMapping("user/count")
    public Response getUserCount() {
        return commonService.getUserCount();
    }

    /**
     * 获取询咨询师总数
     *
     * @return {@link Response}
     */
    @GetMapping("consulter/count")
    public Response getConsulterCount() {
        return commonService.getConsulterCount();
    }

    /**
     * 获取系统中所有的金币存量总计
     *
     * @return {@link Response}
     */
    @GetMapping("coin/total")
    public Response getCoinTotal() {
        return commonService.getCoinTotal();
    }

    /**
     * 获取系统提现记录总计
     *
     * @return {@link Response}
     */
    @GetMapping("settle/total")
    public Response getSettleTotal() {
        return commonService.getSettleTotal();
    }

    /**
     * 获取话题总数
     *
     * @return {@link Response}
     */
    @GetMapping("topic/count")
    public Response getTopicTotal() {
        return commonService.getTopicTotal();
    }

    /**
     * 获取漂流瓶总数
     *
     * @return {@link Response}
     */
    @GetMapping("bottle/count")
    public Response getBottleTotal() {
        return commonService.getBottleTotal();
    }
}
