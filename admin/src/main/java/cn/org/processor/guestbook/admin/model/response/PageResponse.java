package cn.org.processor.guestbook.admin.model.response;

import lombok.Data;

@Data
public class PageResponse {
    private Integer total;
    private Object list;
}
