package cn.org.processor.guestbook.admin.service;

import io.Response;

public interface CommonService {
    Response getUserCount();

    Response getCoinTotal();

    Response getSettleTotal();

    Response getConsulterCount();

    Response getTopicTotal();

    Response getBottleTotal();
}
