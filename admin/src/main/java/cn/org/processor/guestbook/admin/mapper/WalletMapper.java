package cn.org.processor.guestbook.admin.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface WalletMapper {
    long getCoinTotal();
}
