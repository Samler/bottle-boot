package cn.org.processor.guestbook.admin.controller;

import cn.org.processor.guestbook.admin.service.TopicService;
import io.Page;
import io.Response;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@RestController
@RequestMapping("topic")
public class TopicController {
    private final TopicService topicService;

    public TopicController(TopicService topicService) {
        this.topicService = topicService;
    }

    /**
     * 获取话题详情
     *
     * @param id topic id
     * @return {@link Response}
     */
    @GetMapping("{id}")
    public Response detail(@PathVariable @Validated
                         @NotNull(message = "参数错误")
                         @Positive(message = "话题id有误") Integer id) {
        return topicService.detail(id);
    }

    /**
     * 列出话题
     *
     * @param page {@link Page}
     * @return {@link Response}
     */
    @PostMapping("list")
    public Response list(@RequestBody @Validated Page page) {
        return topicService.list(page);
    }

    /**
     * 锁定话题
     *
     * @param id topic id
     * @return {@link Response}
     */
    @PostMapping("lock/{id}")
    public Response lock(@PathVariable @Validated
                             @NotNull(message = "参数错误")
                             @Positive(message = "话题id有误") Integer id) {
        return topicService.lock(id);
    }

    /**
     * 重开话题
     *
     * @param id topic id
     * @return {@link Response}
     */
    @PostMapping("unlock/{id}")
    public Response unlock(@PathVariable @Validated
                         @NotNull(message = "参数错误")
                         @Positive(message = "话题id有误") Integer id) {
        return topicService.unlock(id);
    }
}
