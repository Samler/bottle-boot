package cn.org.processor.guestbook.admin.model.request;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
public class UserChangeRoleRequest {
    @NotNull(message = "参数错误")
    @Positive(message = "用户id有误")
    private Integer id;

    @NotNull(message = "参数错误")
    @Min(value = 0, message = "用户类型有误")
    @Max(value = 1, message = "用户类型有误")
    private Integer role;
}
