package cn.org.processor.guestbook.admin.service.impl;

import cn.org.processor.guestbook.admin.mapper.AdminMapper;
import cn.org.processor.guestbook.admin.model.request.AdminChangePasswordRequest;
import cn.org.processor.guestbook.admin.model.request.AdminLoginRequest;
import cn.org.processor.guestbook.admin.service.AccountService;
import cn.org.processor.guestbook.admin.util.ContextHolderUtil;
import cn.org.processor.guestbook.admin.util.IpUtil;
import cn.org.processor.guestbook.admin.util.ReCaptchaUtil;
import cn.org.processor.guestbook.admin.util.TokenUtil;
import entity.Admin;
import io.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import util.EncryptUtil;

import java.util.Objects;

@Service
@Slf4j
public class AccountServiceImpl implements AccountService {
    private final AdminMapper adminMapper;
    private final ReCaptchaUtil reCaptchaUtil;
    private final TokenUtil tokenUtil;

    public AccountServiceImpl(AdminMapper adminMapper, ReCaptchaUtil reCaptchaUtil, TokenUtil tokenUtil) {
        this.adminMapper = adminMapper;
        this.reCaptchaUtil = reCaptchaUtil;
        this.tokenUtil = tokenUtil;
    }

    @Override
    public Response login(AdminLoginRequest request) {
        // 测试类可绕过验证码
        if (request.getCaptcha() != null && reCaptchaUtil.hasSecret()) {
            boolean verify = reCaptchaUtil.verify(request.getCaptcha(),
                    IpUtil.getIpAddr(ContextHolderUtil.getRequest()));
            if (!verify) {
                return Response.builder().ret(Response.Ret.FAILURE).msg("验证失败").build();
            }
        }

        Admin admin = adminMapper.findByUserName(request.getUsername());
        if (admin == null ||
                !Objects.equals(admin.getPassword(), EncryptUtil.getPasswordEncryptStr(request.getPassword()))) {
            return Response.builder().ret(Response.Ret.FAILURE).msg("用户名或密码错误").build();
        }

        return Response.builder()
                .ret(Response.Ret.SUCCESS)
                .msg("登录成功")
                .data(tokenUtil.getAccessToken(admin))
                .build();
    }

    @Override
    public Response changePassword(AdminChangePasswordRequest request) {
        Admin admin = (Admin) ContextHolderUtil.getCurrentUser();

        if (Objects.equals(request.getOldPassword(), request.getNewPassword())){
            return Response.builder().ret(Response.Ret.FAILURE).msg("新密码不能与旧密码相同").build();
        }

        if (!Objects.equals(request.getNewPassword(), request.getConfirmPassword())) {
            return Response.builder().ret(Response.Ret.FAILURE).msg("两次输入的密码不一致").build();
        }

        Response response = EncryptUtil.checkStrength(request.getNewPassword());
        if (!response.isSuccess()) {
            return response;
        }

        Admin updateItem = new Admin();
        updateItem.setId(admin.getId());
        updateItem.setPassword(request.getNewPassword());
        if (adminMapper.update(updateItem) > 0) {
            return Response.builder().ret(Response.Ret.SUCCESS).msg("修改成功").build();
        }

        log.error("Failed to change password for admin user: {}", admin);
        return Response.builder().ret(Response.Ret.FAILURE).msg("修改失败").build();
    }
}
