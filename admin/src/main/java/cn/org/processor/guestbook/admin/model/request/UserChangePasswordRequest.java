package cn.org.processor.guestbook.admin.model.request;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
public class UserChangePasswordRequest {
    @NotNull(message = "参数错误")
    @Positive(message = "用户id有误")
    private Integer id;

    @NotNull(message = "参数错误")
    private String password;
}
