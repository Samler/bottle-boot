package cn.org.processor.guestbook.admin.util;

import entity.Admin;
import org.springframework.stereotype.Component;
import util.EncryptUtil;

import java.util.HashMap;

/**
 * Token工具类
 */
@Component
public class TokenUtil {
    private final JWTUtil jwtUtil;

    public TokenUtil(JWTUtil jwtUtil) {
        this.jwtUtil = jwtUtil;
    }

    /**
     * 获取登录成功后的access_token
     * @param admin 管理员实体
     * @return accessToken
     */
    public String getAccessToken(Admin admin) {
        return jwtUtil.sign(new HashMap<>() {
            {
                this.put("uid", admin.getId());
                this.put("token", getToken(admin));
            }
        });
    }

    /**
     * 获取用户的token
     * @param user 用户实体
     * @return String
     */
    public static String getToken(Admin user) {
        return EncryptUtil.getMD5("20220101-admin-" + user.getId() + "@" + user.getPassword());
    }

}
