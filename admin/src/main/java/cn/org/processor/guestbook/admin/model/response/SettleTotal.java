package cn.org.processor.guestbook.admin.model.response;

import lombok.Data;

@Data
public class SettleTotal {
    private Long coin;
    private Double money;
}
