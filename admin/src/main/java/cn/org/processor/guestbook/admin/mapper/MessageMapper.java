package cn.org.processor.guestbook.admin.mapper;

import entity.Message;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface MessageMapper {
//    暂时没用上
//    int insert(Message message);
//    int deleteById(int id);
    int update(Message message);
    Message findById(int id);
    int countByBottle();

    int getCount();

    List<Message> list(@Param("start") int start, @Param("offset") int offset);
}
