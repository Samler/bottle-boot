package cn.org.processor.guestbook.admin.service;

import cn.org.processor.guestbook.admin.model.request.UserChangePasswordRequest;
import cn.org.processor.guestbook.admin.model.request.UserChangeRoleRequest;
import entity.UserProfile;
import io.Page;
import io.Response;

public interface UserService {

    Response list(Page page);

    Response lock(Integer id);

    Response unlock(Integer id);

    Response changePassword(UserChangePasswordRequest request);

    Response changeRole(UserChangeRoleRequest request);

    Response changeProfile(UserProfile profile);

    Response detail(Integer id);
}
