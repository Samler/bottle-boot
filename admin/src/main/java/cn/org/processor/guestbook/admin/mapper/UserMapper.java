package cn.org.processor.guestbook.admin.mapper;

import cn.org.processor.guestbook.admin.entity.UserAndProfile;
import entity.User;
import entity.UserProfile;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface UserMapper {
    int getCountByRole(int role);

    int getCount();

    UserAndProfile detail(int id);

    List<UserAndProfile> listUserAndProfile(@Param("start") int start, @Param("offset") int offset);

    User findById(int id);

    List<User> list(@Param("start") int start, @Param("offset") int offset);

    int update(User user);
}
