package cn.org.processor.guestbook.admin.model.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class AdminLoginRequest {
    @NotNull(message = "用户名不能为空")
    private String username;

    @NotNull(message = "密码不能为空")
    private String password;

    @NotNull(message = "参数错误")
    private String captcha;
}
