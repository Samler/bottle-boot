package cn.org.processor.guestbook.admin.entity;

import lombok.Data;

@Data
public class UserAndProfile {
    private Integer id;
    private String username;
    private String password;
    private Integer role;
    private Integer status;
    private String name;
    private String telephone;
    private String mail;
    private String intro;
    private Integer consultPrice;
}
