package cn.org.processor.guestbook.admin.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.Response;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class InterceptorResponseUtil {
    public static void response(HttpServletResponse response, Response result) {
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json; charset=utf-8");
        response.setStatus(200);
        try (PrintWriter out = response.getWriter()){
            ObjectMapper objectMapper = new ObjectMapper();
            out.append(objectMapper.writeValueAsString(result));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
