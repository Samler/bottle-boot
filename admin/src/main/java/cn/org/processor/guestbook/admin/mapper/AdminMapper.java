package cn.org.processor.guestbook.admin.mapper;

import entity.Admin;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AdminMapper {
    Admin findById(int id);
    Admin findByUserName(String username);
    int update(Admin admin);
}
