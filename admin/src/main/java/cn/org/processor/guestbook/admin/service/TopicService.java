package cn.org.processor.guestbook.admin.service;

import io.Page;
import io.Response;

public interface TopicService {
    Response list(Page page);

    Response lock(Integer id);

    Response unlock(Integer id);

    Response detail(Integer id);
}
