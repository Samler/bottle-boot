package cn.org.processor.guestbook.admin.service.impl;

import cn.org.processor.guestbook.admin.entity.UserAndProfile;
import cn.org.processor.guestbook.admin.mapper.UserMapper;
import cn.org.processor.guestbook.admin.mapper.UserProfileMapper;
import cn.org.processor.guestbook.admin.model.request.UserChangePasswordRequest;
import cn.org.processor.guestbook.admin.model.request.UserChangeRoleRequest;
import cn.org.processor.guestbook.admin.model.response.PageResponse;
import cn.org.processor.guestbook.admin.service.UserService;
import entity.User;
import entity.UserProfile;
import io.Page;
import io.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import util.EncryptUtil;
import util.PageUtil;

import java.util.List;

@Service
@Slf4j
public class UserServiceImpl implements UserService {
    private final UserMapper userMapper;
    private final UserProfileMapper userProfileMapper;

    public UserServiceImpl(UserMapper userMapper, UserProfileMapper userProfileMapper) {
        this.userMapper = userMapper;
        this.userProfileMapper = userProfileMapper;
    }

    @Override
    public Response list(Page page) {
        int count = userMapper.getCount();

        int start = PageUtil.getStartRows(page.getPage(), page.getOffset());
        List<UserAndProfile> list = userMapper.listUserAndProfile(start, page.getOffset());
        list.forEach(userAndProfile -> userAndProfile.setPassword(null));

        PageResponse response = new PageResponse();
        response.setTotal(count);
        response.setList(list);

        return Response.builder().ret(Response.Ret.SUCCESS).msg("获取成功").data(response).build();
    }

    @Override
    public Response lock(Integer id) {
        User user = userMapper.findById(id);
        if (user == null) {
            return Response.builder().ret(Response.Ret.FAILURE).msg("用户不存在").build();
        }

        user.setStatus(User.Status.LOCKED);
        if (userMapper.update(user) > 0) {
            return Response.builder().ret(Response.Ret.SUCCESS).msg("操作成功").build();
        }

        log.error("Failed to lock user: {}", user);
        return Response.builder().ret(Response.Ret.FAILURE).msg("操作失败").build();
    }

    @Override
    public Response unlock(Integer id) {
        User user = userMapper.findById(id);
        if (user == null) {
            return Response.builder().ret(Response.Ret.FAILURE).msg("用户不存在").build();
        }

        user.setStatus(User.Status.NORMAL);
        if (userMapper.update(user) > 0) {
            return Response.builder().ret(Response.Ret.SUCCESS).msg("操作成功").build();
        }

        log.error("Failed to lock user: {}", user);
        return Response.builder().ret(Response.Ret.FAILURE).msg("操作失败").build();
    }

    @Override
    public Response changePassword(UserChangePasswordRequest request) {
        User user = userMapper.findById(request.getId());
        if (user == null) {
            return Response.builder().ret(Response.Ret.FAILURE).msg("用户不存在").build();
        }

        user.setPassword(EncryptUtil.getPasswordEncryptStr(request.getPassword()));
        if (userMapper.update(user) > 0) {
            return Response.builder().ret(Response.Ret.SUCCESS).msg("操作成功").build();
        }

        log.error("Failed to change password for user: {}", user);
        return Response.builder().ret(Response.Ret.FAILURE).msg("操作失败").build();
    }

    @Override
    public Response changeRole(UserChangeRoleRequest request) {
        User user = userMapper.findById(request.getId());
        if (user == null) {
            return Response.builder().ret(Response.Ret.FAILURE).msg("用户不存在").build();
        }

        user.setRole(request.getRole());
        if (userMapper.update(user) > 0) {
            return Response.builder().ret(Response.Ret.SUCCESS).msg("操作成功").build();
        }

        log.error("Failed to change role for user: {}", user);
        return Response.builder().ret(Response.Ret.FAILURE).msg("操作失败").build();
    }

    @Override
    public Response changeProfile(UserProfile profile) {
        if (profile.getId() == null) {
            return Response.builder().ret(Response.Ret.FAILURE).msg("请输入用户id").build();
        }

        UserProfile userProfile = userProfileMapper.findById(profile.getId());
        if (userProfile == null) {
            return Response.builder().ret(Response.Ret.FAILURE).msg("用户不存在").build();
        }

        if (userProfileMapper.updateAll(profile) > 0) {
            return Response.builder().ret(Response.Ret.SUCCESS).msg("操作成功").build();
        }

        log.error("Failed to change profile for user: {}", userProfile);
        return Response.builder().ret(Response.Ret.FAILURE).msg("操作失败").build();
    }

    @Override
    public Response detail(Integer id) {
        UserAndProfile detail = userMapper.detail(id);
        if (detail == null) {
            return Response.builder().ret(Response.Ret.FAILURE).msg("用户不存在").build();
        }

        detail.setPassword(null);
        return Response.builder().ret(Response.Ret.SUCCESS).msg("获取成功").data(detail).build();
    }
}
