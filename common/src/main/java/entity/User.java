package entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class User {
    private Integer id;
    private String username;
    @JsonIgnore
    private String password;
    private Integer role;
    private Integer status;

    public static class Role {
        public static final int USER = 0;
        public static final int COUNSELOR = 1;
    }

    public static class Status {
        public static final int NORMAL = 0;
        public static final int LOCKED = 1;
    }
}
