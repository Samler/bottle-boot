package entity;

import lombok.Data;

import java.util.Date;

@Data
public class Topic {
    private Integer id;
    private Integer userId;
    private String title;
    private String content;
    private Date date;
    private Integer status;

    public static class Status {
        public static final int OPEN = 0;
        public static final int CLOSE = 1;
    }
}
