package entity;

import lombok.Data;

import java.util.Date;

@Data
public class Settle {
    private Integer id;
    private Integer userId;
    private Integer coin;
    private Double money;
    private Date date;
}
