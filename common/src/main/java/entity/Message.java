package entity;

import lombok.Data;

import java.util.Date;

@Data
public class Message {
    private Integer id;
    private Integer userId;
    private Integer topicId;
    private Integer type;
    private Integer receiveUserId;
    private Integer pickUserId;
    private String content;
    private Date date;
    private Integer status;
    private Integer replyStatus;

    public static class Type {
        public static final int OPEN = 0;
        public static final int PRIVATE = 1;
        public static final int APPOINT = 2;
    }

    public static class Status {
        public static final int VISIBLE = 0;
        public static final int INVISIBLE = 1;
    }

    public static class ReplyStatus {
        public static final int NO_REPLY = 0;
        public static final int HAS_REPLY = 1;
    }
}
