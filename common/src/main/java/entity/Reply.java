package entity;

import lombok.Data;

import java.util.Date;

@Data
public class Reply {
    private Integer id;
    private Integer userId;
    private Integer messageId;
    private String content;
    private Date date;
    private Integer status;

    public static class Status {
        public static final int NORMAL = 0;
        public static final int SHIELD = 1;
    }
}
