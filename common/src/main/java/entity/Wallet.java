package entity;

import lombok.Data;

@Data
public class Wallet {
    private Integer id;
    private Integer coin;
}
