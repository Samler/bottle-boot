package entity;

import lombok.Data;

@Data
public class UserProfile {
    private Integer id;
    private String name;
    private String telephone;
    private String mail;
    private String intro;
    private Integer consultPrice;
}
