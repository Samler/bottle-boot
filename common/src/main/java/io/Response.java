package io;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Response {
    private int ret;
    private String msg;
    private Object data;

    public static class Ret {
        public static final int SUCCESS = 0;
        public static final int FAILURE = -1;

        public static final int TOKEN_REFRESHED = 10000;
        public static final int TOKEN_INVALID = 10001;
    }

    @JsonIgnore
    public boolean isSuccess() {
        return this.ret == Ret.SUCCESS;
    }
}
