package io;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.Positive;

@Data
public class Page {
    @Positive(message = "页数错误")
    private Integer page;

    @Positive(message = "每页显示的数据条目数量有误")
    @Min(value = 10, message = "每页显示的数据条目不能小于10")
    private Integer offset;
}
