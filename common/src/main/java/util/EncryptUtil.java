package util;

import io.Response;
import org.apache.commons.codec.digest.DigestUtils;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class EncryptUtil {

    public static String getMD5(String str) {
        return DigestUtils.md5Hex(str);
    }

    /**
     * 利用java原生的类实现SHA256加密
     *
     * @param str 加密后的报文
     * @return String by encoded
     */
    public static String getSHA256(String str) {
        MessageDigest messageDigest;
        String encodestr = "";
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(str.getBytes(StandardCharsets.UTF_8));
            encodestr = byte2Hex(messageDigest.digest());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return encodestr;
    }

    /**
     * 将byte转为16进制
     *
     * @param bytes bytes to need translate
     * @return String by hex
     */
    private static String byte2Hex(byte[] bytes) {
        StringBuilder stringBuilder = new StringBuilder();
        String temp;
        for (byte b : bytes) {
            temp = Integer.toHexString(b & 0xFF);
            if (temp.length() == 1) {
                // 1得到一位的进行补0操作
                stringBuilder.append("0");
            }

            stringBuilder.append(temp);
        }

        return stringBuilder.toString();
    }

    /**
     * 随机生成密码
     *
     * @return Rand string
     */
    public static String getRandPassword() {
        String[] str_list = new String[]{
                "1", "2", "3", "4", "5", "6", "7", "8", "9", "0",
                "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
                "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
                "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "-", "_", "=", "+"
        };
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 32; i++)
            sb.append(str_list[(int) (Math.random() * str_list.length)]);

        return sb.toString();
    }

    public static Response checkStrength(String password) {
        Response.ResponseBuilder resp = Response.builder();
        if(password.length() < 6)
            return resp.ret(Response.Ret.FAILURE).msg("密码长度至少6位").build();

        int couter = 0;
        // 包含数字
        if(password.matches(".*\\d.*"))
            couter++;
        // 包含大写字母
        if(password.matches(".*[A-Z].*"))
            couter++;
        // 包含小写字母
        if(password.matches(".*[a-z].*"))
            couter++;
        // 包含符号
        if(password.matches(".*[^0-9a-zA-Z].*"))
            couter++;

        if(couter < 2)
            return resp.ret(Response.Ret.FAILURE).msg("数字、大写字母、小写字母、符号中至少要包含两种").build();


        return resp.ret(Response.Ret.SUCCESS).build();
    }

    public static String getPasswordEncryptStr(String pwd) {
        return getMD5(getSHA256(pwd) + "#20220101@GBook");
    }
}
