package util;

import io.Page;

public class PageUtil {
    public static int getStartRows(int page, int offset) {
        if (page <= 0) {
            page = 1;
        }

        return (page - 1) * offset;
    }

    public static long getStartRows(long page, int offset) {
        if (page <= 0) {
            page = 1;
        }

        return (page - 1) * offset;
    }

    public static int getStartRows(Page page) {
        return getStartRows(page.getPage(), page.getOffset());
    }
}
