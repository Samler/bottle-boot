package cn.org.processor.guestbook.api.service;

import cn.org.processor.guestbook.api.ApiApplicationTests;
import cn.org.processor.guestbook.api.model.request.TopicCloseRequest;
import cn.org.processor.guestbook.api.model.request.TopicCreateRequest;
import entity.User;
import io.Page;
import io.Response;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class TopicTests extends ApiApplicationTests {
    @Autowired
    private TopicService topicService;
    private User user;

    @BeforeAll
    void init() {
        user = super.createTestUser(User.Role.COUNSELOR);
        super.mockLogin(user);
    }

    @AfterAll
    void destroy() {
        super.deleteTestUser(user);
    }

    @Test
    public void list() {
        Page page = new Page();
        page.setPage(1);
        page.setOffset(10);

        Response response = topicService.list(page);
        Assertions.assertTrue(response.isSuccess(), response.getMsg());
        System.out.println(response);
    }

    @Test
    public void listMe() {
        Page page = new Page();
        page.setPage(1);
        page.setOffset(10);

        Response response = topicService.listMe(page);
        Assertions.assertTrue(response.isSuccess(), response.getMsg());
        System.out.println(response);
    }

    @Test
    public void create() {
        TopicCreateRequest request = new TopicCreateRequest();
        request.setTitle("测试话题");
        request.setContent("这是一个测试话题");

        Response response = topicService.create(request);
        Assertions.assertTrue(response.isSuccess(), response.getMsg());
        System.out.println(response);
    }

    @Test
    public void close() {
        TopicCreateRequest createRequest = new TopicCreateRequest();
        createRequest.setTitle("测试话题");
        createRequest.setContent("这是一个测试话题");

        Response response = topicService.create(createRequest);
        Assertions.assertTrue(response.isSuccess(), response.getMsg());

        int id = Integer.parseInt(String.valueOf(response.getData()));
        TopicCloseRequest closeRequest = new TopicCloseRequest();
        closeRequest.setId(id);

        response = topicService.close(closeRequest);
        Assertions.assertTrue(response.isSuccess(), response.getMsg());
        System.out.println(response);
    }
}
