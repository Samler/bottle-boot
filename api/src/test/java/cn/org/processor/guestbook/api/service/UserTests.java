package cn.org.processor.guestbook.api.service;

import cn.org.processor.guestbook.api.ApiApplicationTests;
import cn.org.processor.guestbook.api.model.request.UserLoginRequest;
import cn.org.processor.guestbook.api.model.request.UserRegisterRequest;
import entity.User;
import io.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@Transactional
public class UserTests extends ApiApplicationTests {
    @Autowired
    private UserService userService;

    @Test
    public void register() {
        UserRegisterRequest request = new UserRegisterRequest();
        request.setUsername(super.getTestUsernameByRand());
        request.setPassword(ApiApplicationTests.TEST_USER_PASSWORD);
        request.setConfirmPassword(ApiApplicationTests.TEST_USER_PASSWORD);
        request.setRole(entity.User.Role.USER);

        Response response = userService.register(request);
        Assertions.assertTrue(response.isSuccess(), "Failed to register for test user: " + response.getMsg());
        System.out.println(response);
    }

    @Test
    public void login() {
        User user = super.createTestUser(User.Role.USER);

        UserLoginRequest request = new UserLoginRequest();
        request.setUsername(user.getUsername());
        request.setPassword(ApiApplicationTests.TEST_USER_PASSWORD);

        Response response = userService.login(request);
        Assertions.assertTrue(response.isSuccess(), "Failed to login for test user: " + response.getMsg());
        System.out.println(response);
    }
}
