package cn.org.processor.guestbook.api.net;

import cn.org.processor.guestbook.api.util.ReCaptchaUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;

@SpringBootTest
public class ReCaptcha {
    @Autowired
    private ReCaptchaUtil reCaptchaUtil;

    @Test
    public void sendRequest() {
        Map request = reCaptchaUtil.request("test", "::1");
        List<String> err = (List) request.get("error-codes");

        Assertions.assertNotSame("reCAPTCHA v3 配置错误, 请查阅: https://developers.google.com/recaptcha/docs/verify",
                "invalid-input-response", err.get(0));
    }
}
