package cn.org.processor.guestbook.api;

import cn.org.processor.guestbook.api.mapper.UserMapper;
import cn.org.processor.guestbook.api.mapper.UserProfileMapper;
import cn.org.processor.guestbook.api.util.ContextHolderUtil;
import entity.User;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletRequest;
import util.EncryptUtil;

@SpringBootTest(classes = ApiApplication.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ApiApplicationTests {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserProfileMapper userProfileMapper;

    public static final String TEST_USER_PASSWORD = "GBook-test-20220101";

    protected String getTestUsernameByRand() {
        return "GBookTest#" + RandomString.make(5);
    }

    protected User createTestUser(int role) {
        User user = new User();
        user.setUsername(getTestUsernameByRand());
        user.setPassword(EncryptUtil.getPasswordEncryptStr(TEST_USER_PASSWORD));
        user.setRole(role);
        user.setStatus(User.Status.NORMAL);

        Assertions.assertTrue(userMapper.create(user) > 0, "Failed to create the test user");
        Assertions.assertTrue(userProfileMapper.createById(user.getId()) > 0,
                "Failed to create the user profile for test user");
        return user;
    }

    protected void deleteTestUser(User user) {
        userMapper.deleteById(user.getId());
        userProfileMapper.deleteById(user.getId());
    }

    protected void mockLogin(User user) {
        MockHttpServletRequest httpServletRequest = new MockHttpServletRequest();
        httpServletRequest.setCharacterEncoding("utf-8");
        httpServletRequest.setAttribute("current_user", user);

        ContextHolderUtil.mockHttpServletRequest = httpServletRequest;
    }
}
