package cn.org.processor.guestbook.api.service;

import cn.org.processor.guestbook.api.ApiApplicationTests;
import cn.org.processor.guestbook.api.model.request.MessageCreateRequest;
import cn.org.processor.guestbook.api.model.request.MessageDiscardRequest;
import cn.org.processor.guestbook.api.model.request.MessageReplyRequest;
import cn.org.processor.guestbook.api.model.request.TopicCreateRequest;
import entity.Message;
import entity.User;
import io.Page;
import io.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class MessageTests extends ApiApplicationTests {
    @Autowired
    private MessageService messageService;
    @Autowired
    private TopicService topicService;

    private User user_1;
    private User user_2;
    private User user_3;

    @BeforeAll
    public void init() {
        user_1 = super.createTestUser(User.Role.USER);
        user_2 = super.createTestUser(User.Role.USER);
        user_3 = super.createTestUser(User.Role.COUNSELOR);
    }

    @Test
    public void createOpenMessage() {
        super.mockLogin(user_1);

        MessageCreateRequest request = new MessageCreateRequest();
        request.setType(Message.Type.OPEN);
        request.setContent("这是一条公开的测试留言");
        Response response = messageService.create(request);

        Assertions.assertTrue(response.isSuccess(),
                "Failed to create the public message of user, message: " + response);
        System.out.println(response);
    }

    @Test
    public void createPrivateMessage() {
        super.mockLogin(user_1);

        MessageCreateRequest request = new MessageCreateRequest();
        request.setType(Message.Type.PRIVATE);
        request.setContent("这是一条私密的测试留言");
        Response response = messageService.create(request);

        Assertions.assertTrue(response.isSuccess(),
                "Failed to create the message of user private, response: " + response);
        System.out.println(response);
    }

    @Test
    public void createAppointMessage() {
        super.mockLogin(user_1);

        MessageCreateRequest request = new MessageCreateRequest();
        request.setType(Message.Type.APPOINT);
        request.setReceiveUserId(user_3.getId());
        request.setContent("这是一条指定接收者的测试留言");
        Response response = messageService.create(request);
        Assertions.assertTrue(response.isSuccess(),
                "Failed to create the message of appoint user, message: " + response);
        System.out.println(response);
    }

    @Test
    public void createTopicMessage() {
        super.mockLogin(user_3);
        TopicCreateRequest topicCreateRequest = new TopicCreateRequest();
        topicCreateRequest.setTitle("这是一个测试话题");
        topicCreateRequest.setContent("这是一个测试话题的内容");

        Response response = topicService.create(topicCreateRequest);
        Assertions.assertTrue(response.isSuccess(),
                "Failed to create the topic, response: " + response);
        System.out.println(response);

        MessageCreateRequest request = new MessageCreateRequest();
        request.setType(Message.Type.OPEN);
        request.setTopicId(Integer.parseInt(response.getData().toString()));
        request.setContent("这是一条测试话题下的测试留言");

        response = messageService.create(request);
        Assertions.assertTrue(response.isSuccess(),
                "Failed to create the topic message, response: " + response);
        System.out.println(response);
    }

    @Test
    public void pickMessage() {
        super.mockLogin(user_1);
        MessageCreateRequest messageCreateRequest = new MessageCreateRequest();
        messageCreateRequest.setType(Message.Type.OPEN);
        messageCreateRequest.setContent("这是一条公开的测试留言");

        Response response = messageService.create(messageCreateRequest);
        Assertions.assertTrue(response.isSuccess(),
                "Failed to create the public message of user, response: " + response);
        System.out.println(response);

        super.mockLogin(user_2);
        response = messageService.pick();
        Assertions.assertTrue(response.isSuccess(),
                "Failed to pick the message, response: " + response);
        System.out.println(response);

        response = messageService.detail(Integer.valueOf(response.getData().toString()));
        Assertions.assertTrue(response.isSuccess(),
                "Failed to detail the message, response: " + response);
        System.out.println(response);
    }

    @Test
    public void discardMessage() {
        super.mockLogin(user_1);
        MessageCreateRequest messageCreateRequest = new MessageCreateRequest();
        messageCreateRequest.setType(Message.Type.OPEN);
        messageCreateRequest.setContent("这是一条公开的测试留言");

        Response response = messageService.create(messageCreateRequest);
        Assertions.assertTrue(response.isSuccess(),
                "Failed to create the public message of user, response: " + response);
        System.out.println(response);

        super.mockLogin(user_2);
        response = messageService.pick();
        Assertions.assertTrue(response.isSuccess(),
                "Failed to pick the message, response: " + response);
        System.out.println(response);

        MessageDiscardRequest messageDiscardRequest = new MessageDiscardRequest();
        messageDiscardRequest.setId(Integer.valueOf(response.getData().toString()));

        response = messageService.discard(messageDiscardRequest);
        Assertions.assertTrue(response.isSuccess(),
                "Failed to discard the message, response: " + response);
        System.out.println(response);
    }

    @Test
    public void replyMessage() {
        super.mockLogin(user_1);
        MessageCreateRequest messageCreateRequest = new MessageCreateRequest();
        messageCreateRequest.setType(Message.Type.OPEN);
        messageCreateRequest.setContent("这是一条公开的测试留言");

        Response response = messageService.create(messageCreateRequest);
        Assertions.assertTrue(response.isSuccess(),
                "Failed to create the public message of user, response: " + response);
        System.out.println(response);

        super.mockLogin(user_2);
        response = messageService.pick();
        Assertions.assertTrue(response.isSuccess(),
                "Failed to pick the message, response: " + response);
        System.out.println(response);

        MessageReplyRequest messageReplyRequest = new MessageReplyRequest();
        messageReplyRequest.setMessageId(Integer.valueOf(response.getData().toString()));
        messageReplyRequest.setContent("这是一条测试回复");
        response = messageService.reply(messageReplyRequest);
        Assertions.assertTrue(response.isSuccess(),
                "Failed to reply the message, response: " + response);
        System.out.println(response);
    }

    @Test
    public void listByCurrentUser() {
        super.mockLogin(user_1);
        MessageCreateRequest messageCreateRequest = new MessageCreateRequest();
        messageCreateRequest.setType(Message.Type.OPEN);
        messageCreateRequest.setContent("这是一条公开的测试留言");

        Response response = messageService.create(messageCreateRequest);
        Assertions.assertTrue(response.isSuccess(),
                "Failed to create the public message of user, response: " + response);
        System.out.println(response);

        super.mockLogin(user_2);
        response = messageService.create(messageCreateRequest);
        Assertions.assertTrue(response.isSuccess(),
                "Failed to create the public message of user, response: " + response);
        System.out.println(response);

        Page page = new Page();
        page.setPage(1);
        page.setOffset(10);

        response = messageService.listByCurrentUser(page);
        Assertions.assertTrue(response.isSuccess(),
                "Failed to list the message of current user, response: " + response);
    }

    @Test
    public void listByReceive() {
        super.mockLogin(user_1);
        MessageCreateRequest messageCreateRequest = new MessageCreateRequest();
        messageCreateRequest.setReceiveUserId(user_3.getId());
        messageCreateRequest.setType(Message.Type.APPOINT);
        messageCreateRequest.setContent("这是一条指定接收人的测试留言");

        Response response = messageService.create(messageCreateRequest);
        Assertions.assertTrue(response.isSuccess(),
                "Failed to create the public message of user, response: " + response);
        System.out.println(response);

        super.mockLogin(user_2);
        response = messageService.create(messageCreateRequest);
        Assertions.assertTrue(response.isSuccess(),
                "Failed to create the public message of user, response: " + response);
        System.out.println(response);

        super.mockLogin(user_3);
        Page page = new Page();
        page.setPage(1);
        page.setOffset(10);

        response = messageService.listByReceive(page);
        Assertions.assertTrue(response.isSuccess(),
                "Failed to list the message of receive user, response: " + response);
    }

    @Test
    public void listByPick() {
        super.mockLogin(user_1);
        MessageCreateRequest messageCreateRequest = new MessageCreateRequest();
        messageCreateRequest.setType(Message.Type.OPEN);
        messageCreateRequest.setContent("这是一条公开的测试留言");

        Response response = messageService.create(messageCreateRequest);
        Assertions.assertTrue(response.isSuccess(),
                "Failed to create the public message of user, response: " + response);
        System.out.println(response);

        super.mockLogin(user_2);
        response = messageService.pick();
        Assertions.assertTrue(response.isSuccess(),
                "Failed to pick the message, response: " + response);
        System.out.println(response);

        Page page = new Page();
        page.setPage(1);
        page.setOffset(10);

        response = messageService.listByPick(page);
        Assertions.assertTrue(response.isSuccess(),
                "Failed to get the message list of pick, response: " + response);
        System.out.println(response);
    }
}
