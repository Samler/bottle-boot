package cn.org.processor.guestbook.api.service;

import cn.org.processor.guestbook.api.ApiApplicationTests;
import cn.org.processor.guestbook.api.model.request.ProfileUpdateRequest;
import cn.org.processor.guestbook.api.model.request.UpdateConsultPriceRequest;
import entity.User;
import io.Response;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UserProfileTests extends ApiApplicationTests {
    @Autowired
    private UserProfileService userProfileService;
    private User user;

    @BeforeAll
    void init() {
        user = super.createTestUser(User.Role.COUNSELOR);
        super.mockLogin(user);
    }

    @AfterAll
    void destroy() {
        super.deleteTestUser(user);
    }

    @Test
    @Order(1)
    public void update() {
        ProfileUpdateRequest request = new ProfileUpdateRequest();
        request.setName("测试用户");
        request.setTelephone("18888888888");
        request.setMail("test@example.com");
        request.setIntro("这只是一个平平无奇的测试账户");

        Response response = userProfileService.update(request);
        Assertions.assertTrue(response.isSuccess(), "Failed to update the profile for test user");
        System.out.println(response);
    }

    @Test
    @Order(2)
    public void setConsultPrice() {
        UpdateConsultPriceRequest request = new UpdateConsultPriceRequest();
        request.setPrice(15);

        Response response = userProfileService.setConsultPrice(request);
        Assertions.assertTrue(response.isSuccess(), "Failed to update the consult price for test user");
        System.out.println(response);
    }

    @Test
    @Order(3)
    public void show() {
        Response response = userProfileService.getProfile();
        Assertions.assertTrue(response.isSuccess(), "Failed to get the profile for test user");
        System.out.println(response);
    }

    @Test
    @Order(4)
    public void showById() {
        Response response = userProfileService.getProfile(user.getId());
        Assertions.assertTrue(response.isSuccess(), "Failed to get the profile for test user: " + user.getId());
        System.out.println(response);
    }
}
