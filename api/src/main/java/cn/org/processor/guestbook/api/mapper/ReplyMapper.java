package cn.org.processor.guestbook.api.mapper;

import entity.Reply;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ReplyMapper {
    int create(Reply reply);
    List<Reply> findByMessageId(int messageId);
}
