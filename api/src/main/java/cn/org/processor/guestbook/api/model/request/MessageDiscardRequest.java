package cn.org.processor.guestbook.api.model.request;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
public class MessageDiscardRequest {
    @NotNull(message = "参数错误")
    @Positive(message = "留言id有误")
    private Integer id;
}
