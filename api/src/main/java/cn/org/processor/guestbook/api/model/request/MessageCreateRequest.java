package cn.org.processor.guestbook.api.model.request;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class MessageCreateRequest {
    private Integer topicId;

    @NotNull(message = "参数错误")
    private Integer type;

    private Integer receiveUserId;

    @NotNull(message = "参数错误")
    @NotBlank(message = "内容不能为空")
    @Length(max = 500, message = "内容不能超过500个字符")
    private String content;
}
