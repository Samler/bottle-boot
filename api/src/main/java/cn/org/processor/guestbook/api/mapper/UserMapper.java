package cn.org.processor.guestbook.api.mapper;

import entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {
    User findById(int id);
    User findByUsername(String username);
    int create(User user);
    int deleteById(int id);
    List<User> findByIds(List<Integer> userIds);
    int updatePassword(User user);
}
