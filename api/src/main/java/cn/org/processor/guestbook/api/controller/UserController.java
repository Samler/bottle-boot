package cn.org.processor.guestbook.api.controller;

import annotation.IgnoreTokenCheck;
import cn.org.processor.guestbook.api.model.request.UserChangePasswordRequest;
import cn.org.processor.guestbook.api.model.request.UserLoginRequest;
import cn.org.processor.guestbook.api.model.request.UserRegisterRequest;
import cn.org.processor.guestbook.api.service.UserService;
import io.Response;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("user")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * 用户登录
     *
     * @param request {@link UserLoginRequest}
     * @return {@link Response}
     */
    @IgnoreTokenCheck
    @PostMapping("login")
    public Response login(@RequestBody @Validated UserLoginRequest request) {
        return userService.login(request);
    }

    /**
     * 用户注册
     *
     * @param request {@link UserRegisterRequest}
     * @return {@link Response}
     */
    @IgnoreTokenCheck
    @PostMapping("register")
    public Response register(@RequestBody @Validated UserRegisterRequest request) {
        return userService.register(request);
    }

    /**
     * 修改用户的密码
     *
     * @param request {@link UserChangePasswordRequest}
     * @return {@link Response}
     */
    @PostMapping("password")
    public Response changePassword(@RequestBody @Validated UserChangePasswordRequest request) {
        return userService.changePassword(request);
    }

    /**
     * 获取登录状态
     * 如果access_token有效，则返回success，否则将会被拦截器拦截
     *
     * @return {@link Response}
     */
    @GetMapping("login_status")
    public Response isLogin() {
        return Response.builder().ret(Response.Ret.SUCCESS).msg("已登录").build();
    }

    /**
     * 获取用户基础信息
     *
     * @return {@link Response}
     */
    @GetMapping("meta")
    public Response getMeta() {
        return userService.getMeta();
    }
}
