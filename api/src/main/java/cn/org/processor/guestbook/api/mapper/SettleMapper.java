package cn.org.processor.guestbook.api.mapper;

import entity.Settle;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SettleMapper {
    int create(Settle settle);
    List<Settle> listByUserId(@Param("userId") int userId,
                              @Param("start") int start, @Param("offset") int offset);
}
