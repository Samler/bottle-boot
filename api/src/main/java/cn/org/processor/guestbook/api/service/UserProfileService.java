package cn.org.processor.guestbook.api.service;

import cn.org.processor.guestbook.api.model.request.ProfileUpdateRequest;
import cn.org.processor.guestbook.api.model.request.UpdateConsultPriceRequest;
import io.Response;

public interface UserProfileService {
    Response getProfile();

    Response getProfile(Integer id);

    Response update(ProfileUpdateRequest request);

    Response setConsultPrice(UpdateConsultPriceRequest price);
}
