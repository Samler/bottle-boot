package cn.org.processor.guestbook.api.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties("jwt")
public class JWTConfig {
    private Integer expireDay;
    private Integer refreshDay;
    private String secretKey;
}
