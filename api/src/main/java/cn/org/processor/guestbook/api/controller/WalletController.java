package cn.org.processor.guestbook.api.controller;

import cn.org.processor.guestbook.api.model.request.WalletRechargeRequest;
import cn.org.processor.guestbook.api.model.request.WalletSettleRequest;
import cn.org.processor.guestbook.api.service.WalletService;
import io.Page;
import io.Response;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("wallet")
public class WalletController {
    private final WalletService walletService;

    public WalletController(WalletService walletService) {
        this.walletService = walletService;
    }

    /**
     * 查看我的钱包
     *
     * @return {@link Response}
     */
    @GetMapping("")
    public Response getWallet() {
        return walletService.getWallet();
    }

    /**
     * 充值金币
     *
     * @param request {@link WalletRechargeRequest}
     * @return {@link Response}
     */
    @PostMapping("recharge")
    public Response recharge(@RequestBody @Validated WalletRechargeRequest request) {
        return walletService.recharge(request);
    }

    /**
     * 金币提现
     *
     * @param request {@link WalletSettleRequest}
     * @return {@link Response}
     */
    @PostMapping("settle")
    public Response settle(@RequestBody @Validated WalletSettleRequest request) {
        return walletService.settle(request);
    }

    /**
     * 提现记录
     *
     * @param page {@link Page}
     * @return {@link Response}
     */
    @PostMapping("settle/log")
    public Response settleLog(@RequestBody @Validated Page page) {
        return walletService.settleLog(page);
    }
}
