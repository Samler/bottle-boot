package cn.org.processor.guestbook.api.model.request;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

@Data
public class UserRegisterRequest {
    @NotNull(message = "用户名不能为空")
    @NotBlank(message = "用户名不能为空")
    private String username;

    @NotNull(message = "密码不能为空")
    @NotBlank(message = "密码不能为空")
    private String password;

    @NotNull(message = "确认密码不能为空")
    @NotBlank(message = "确认密码不能为空")
    private String confirmPassword;

    @NotNull(message = "注册类型不能为空")
    @PositiveOrZero(message = "注册类型参数有误")
    @Max(value = 1, message = "注册类型参数有误")
    private Integer role;

    @NotNull(message = "参数错误")
    @NotBlank(message = "参数错误")
    private String captcha;
}
