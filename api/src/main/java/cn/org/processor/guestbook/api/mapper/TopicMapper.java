package cn.org.processor.guestbook.api.mapper;

import entity.Topic;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface TopicMapper {
    int create(Topic topic);

    int update(Topic topic);

    Topic findById(int id);

    Topic findByTitle(String title);

    List<Topic> list(@Param("start") int start, @Param("offset") int offset);

    List<Topic> listByUserId(@Param("userId") int id, @Param("start") int start, @Param("offset") int offset);
}
