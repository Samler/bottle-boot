package cn.org.processor.guestbook.api.model.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UserChangePasswordRequest {
    @NotNull(message = "旧密码不能为空")
    private String oldPassword;

    @NotNull(message = "新密码不能为空")
    private String newPassword;

    @NotNull(message = "确认密码不能为空")
    private String confirmPassword;
}
