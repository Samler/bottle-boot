package cn.org.processor.guestbook.api.model.request;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
public class TopicCloseRequest {
    @NotNull(message = "话题id不能为空")
    @Positive(message = "话题id有误")
    private Integer id;
}
