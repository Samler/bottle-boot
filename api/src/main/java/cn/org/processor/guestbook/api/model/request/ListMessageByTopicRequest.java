package cn.org.processor.guestbook.api.model.request;

import io.Page;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
public class ListMessageByTopicRequest {
    @NotNull(message = "参数错误")
    @Positive(message = "话题id有误")
    private Integer topicId;

    @NotNull
    @Valid
    private Page page;
}
