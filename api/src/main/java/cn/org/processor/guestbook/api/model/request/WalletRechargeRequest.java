package cn.org.processor.guestbook.api.model.request;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
public class WalletRechargeRequest {
    @NotNull(message = "参数错误")
    @Positive(message = "最低充值1金币")
    @Max(value = 10000, message = "最多充值10000金币")
    private Integer coin;
}
