package cn.org.processor.guestbook.api.model.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
public class MessageReplyRequest {
    @NotNull(message = "参数错误")
    @Positive(message = "留言id有误")
    private Integer messageId;

    @NotNull(message = "参数错误")
    @NotBlank(message = "回复内容不能为空")
    private String content;
}
