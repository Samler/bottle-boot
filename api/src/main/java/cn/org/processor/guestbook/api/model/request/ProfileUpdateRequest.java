package cn.org.processor.guestbook.api.model.request;

import lombok.Data;

@Data
public class ProfileUpdateRequest {
    private String name;
    private String telephone;
    private String mail;
    private String intro;
}
