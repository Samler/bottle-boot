package cn.org.processor.guestbook.api.service;

import cn.org.processor.guestbook.api.model.request.WalletRechargeRequest;
import cn.org.processor.guestbook.api.model.request.WalletSettleRequest;
import io.Page;
import io.Response;

public interface WalletService {
    Response getWallet();

    Response recharge(WalletRechargeRequest request);

    Response settle(WalletSettleRequest request);

    Response settleLog(Page page);
}
