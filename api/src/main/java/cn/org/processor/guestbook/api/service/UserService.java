package cn.org.processor.guestbook.api.service;

import cn.org.processor.guestbook.api.model.request.UserChangePasswordRequest;
import cn.org.processor.guestbook.api.model.request.UserLoginRequest;
import cn.org.processor.guestbook.api.model.request.UserRegisterRequest;
import io.Response;

public interface UserService {

    Response login(UserLoginRequest request);

    Response register(UserRegisterRequest request);

    Response changePassword(UserChangePasswordRequest request);

    Response getMeta();
}
