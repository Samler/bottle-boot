package cn.org.processor.guestbook.api.mapper;

import entity.Message;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface MessageMapper {
    int create(Message message);

    int update(Message message);

    Message findById(int id);

    List<Message> listByUserId(@Param("userId") int userId,
                               @Param("start") int start, @Param("offset") int offset);

    List<Message> listByUserIdAndStatus(@Param("userId") int userId, @Param("status") int status,
                                        @Param("start") int start, @Param("offset") int offset);

    List<Message> listByTopicId(@Param("topicId") int topicId,
                                @Param("start") int start, @Param("offset") int offset);

    List<Message> listByTopicIdAndStatus(@Param("topicId") int topicId, @Param("status") int status,
                                         @Param("start") int start, @Param("offset") int offset);

    List<Message> listByReceiveUserId(@Param("receiveUserId") int receiveUserId,
                                      @Param("start") int start, @Param("offset") int offset);

    List<Message> listByReceiveUserIdAndStatus(@Param("receiveUserId") int receiveUserId, @Param("status") int status,
                                               @Param("start") int start, @Param("offset") int offset);

    List<Message> findByPickUserIdAndNoReply(int userId);

    Message findOneInWaitPick(int userId);

    int updateForDiscard(int id);

    List<Message> listByPick(@Param("pickUserId") Integer pickUserId,
                             @Param("start") int start, @Param("offset") int offset);

    List<Message> listByTopic(@Param("topicId") int topicId,
                              @Param("start") int start, @Param("offset") Integer offset);
}
