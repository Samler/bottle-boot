package cn.org.processor.guestbook.api.model.request;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class TopicCreateRequest {
    @NotNull(message = "标题不能为空")
    @NotBlank(message = "标题不能为空")
    @Length(max = 30, message = "标题长度必须在30以内")
    private String title;

    @NotNull(message = "内容不能为空")
    @NotBlank(message = "内容不能为空")
    private String content;
}
