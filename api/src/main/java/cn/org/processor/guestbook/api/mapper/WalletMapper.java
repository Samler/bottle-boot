package cn.org.processor.guestbook.api.mapper;

import entity.Wallet;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface WalletMapper {
    int createById(int id);
    Wallet findById(int id);
    int update(Wallet wallet);
}
