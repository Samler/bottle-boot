package cn.org.processor.guestbook.api.service;

import cn.org.processor.guestbook.api.model.request.TopicCloseRequest;
import cn.org.processor.guestbook.api.model.request.TopicCreateRequest;
import io.Page;
import io.Response;

public interface TopicService {
    Response list(Page page);

    Response listMe(Page page);

    Response create(TopicCreateRequest request);

    Response close(TopicCloseRequest request);

    Response detail(Integer id);
}
