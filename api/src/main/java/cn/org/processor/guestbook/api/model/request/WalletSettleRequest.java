package cn.org.processor.guestbook.api.model.request;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
public class WalletSettleRequest {
    @NotNull(message = "参数错误")
    @Positive(message = "金币输入错误")
    @Min(value = 10, message = "提现至少需要10金币")
    @Max(value = 10000, message = "单次最大提现10000金币")
    private Integer coin;
}
