package cn.org.processor.guestbook.api.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties("recaptcha")
public class ReCaptchaConfig {
    private String secret;
}
