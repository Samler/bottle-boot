package cn.org.processor.guestbook.api.controller;

import annotation.UserRoleIs;
import annotation.UserStatusCheck;
import cn.org.processor.guestbook.api.model.request.ProfileUpdateRequest;
import cn.org.processor.guestbook.api.model.request.UpdateConsultPriceRequest;
import cn.org.processor.guestbook.api.service.UserProfileService;
import entity.User;
import io.Response;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.*;

@RestController
@RequestMapping("profile")
@UserStatusCheck
public class ProfileController {
    private final UserProfileService userProfileService;

    public ProfileController(UserProfileService userProfileService) {
        this.userProfileService = userProfileService;
    }

    /**
     * 我的信息
     *
     * @return {@link Response}
     */
    @GetMapping("")
    public Response get() {
        return userProfileService.getProfile();
    }

    /**
     * 他人的信息
     *
     * @param id user id
     * @return {@link Response}
     */
    @GetMapping("{id}")
    public Response get(@PathVariable Integer id) {
        return userProfileService.getProfile(id);
    }

    /**
     * 更新我的信息
     *
     * @param request {@link ProfileUpdateRequest}
     * @return {@link Response}
     */
    @PostMapping("")
    public Response update(@RequestBody @Validated ProfileUpdateRequest request) {
        return userProfileService.update(request);
    }

    /**
     * 更新单次咨询价格
     * 接口仅限角色为咨询师的用户使用
     *
     * @param request {@link UpdateConsultPriceRequest}
     * @return {@link Response}
     */
    @PostMapping("consult_price")
    @UserRoleIs(User.Role.COUNSELOR)
    public Response setConsultPrice(@RequestBody @Validated UpdateConsultPriceRequest request) {
        return userProfileService.setConsultPrice(request);
    }

}
