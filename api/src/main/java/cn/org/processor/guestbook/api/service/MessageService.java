package cn.org.processor.guestbook.api.service;

import cn.org.processor.guestbook.api.model.request.ListMessageByTopicRequest;
import cn.org.processor.guestbook.api.model.request.MessageCreateRequest;
import cn.org.processor.guestbook.api.model.request.MessageDiscardRequest;
import cn.org.processor.guestbook.api.model.request.MessageReplyRequest;
import io.Page;
import io.Response;

public interface MessageService {
    Response create(MessageCreateRequest request);

    Response listByCurrentUser(Page page);

    Response listByReceive(Page page);

    Response pick();

    Response discard(MessageDiscardRequest request);

    Response reply(MessageReplyRequest request);

    Response detail(Integer id);

    Response listByPick(Page page);

    Response listByTopic(ListMessageByTopicRequest request);
}
