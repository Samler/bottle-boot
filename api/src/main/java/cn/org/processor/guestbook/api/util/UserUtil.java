package cn.org.processor.guestbook.api.util;

import entity.User;
import org.springframework.stereotype.Component;
import util.EncryptUtil;

import java.util.HashMap;

/**
 * 用户工具类
 */
@Component
public class UserUtil {
    private final JWTUtil jwtUtil;

    public UserUtil(JWTUtil jwtUtil) {
        this.jwtUtil = jwtUtil;
    }

    /**
     * 获取登录成功后的access_token
     * @param user 用户实体
     * @return accessToken
     */
    public String getAccessToken(User user) {
        return jwtUtil.sign(new HashMap<>() {
            {
                this.put("uid", user.getId());
                this.put("token", getToken(user));
            }
        });
    }

    /**
     * 获取用户的token
     * @param user 用户实体
     * @return String
     */
    public static String getToken(User user) {
        return EncryptUtil.getMD5("20220101-" + user.getId() + "@" + user.getPassword());
    }

}
