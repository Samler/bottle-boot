package cn.org.processor.guestbook.api.controller;

import annotation.IgnoreTokenCheck;
import io.Response;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@IgnoreTokenCheck
@RestController
@RequestMapping("${server.error.path:${error.path:/error}}")
public class ErrorController implements org.springframework.boot.web.servlet.error.ErrorController {

    @RequestMapping
    public Response doHandleError(HttpServletRequest request, HttpServletResponse resp) {
        int code = resp.getStatus();
        String msg;

        if (code == 404) {
            msg = "资源不存在，可能是访问的路径或方式有误";
        } else if (code == 405) {
            msg = "该资源无法使用" + request.getMethod() + "方法进行请求";
        } else if (code == 403) {
            msg = "资源拒绝访问";
        } else {
            msg = "服务器错误";
        }

        return Response.builder().ret(Response.Ret.FAILURE).msg(msg).build();
    }
}
