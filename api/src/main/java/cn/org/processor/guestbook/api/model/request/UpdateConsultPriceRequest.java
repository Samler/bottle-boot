package cn.org.processor.guestbook.api.model.request;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

@Data
public class UpdateConsultPriceRequest {
    @PositiveOrZero(message = "定价最低0金币")
    @Max(value = 1000, message = "定价不可高于1000金币")
    private Integer price;
}
