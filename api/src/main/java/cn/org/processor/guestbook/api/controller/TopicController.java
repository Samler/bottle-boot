package cn.org.processor.guestbook.api.controller;

import annotation.UserRoleIs;
import annotation.UserStatusCheck;
import cn.org.processor.guestbook.api.model.request.TopicCloseRequest;
import cn.org.processor.guestbook.api.model.request.TopicCreateRequest;
import cn.org.processor.guestbook.api.service.TopicService;
import entity.User;
import io.Page;
import io.Response;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@RestController
@RequestMapping("topic")
@UserStatusCheck
public class TopicController {
    private final TopicService topicService;

    public TopicController(TopicService topicService) {
        this.topicService = topicService;
    }

    @GetMapping("{id}")
    public Response detail(@PathVariable @Validated
                               @NotNull(message = "参数错误")
                               @Positive(message = "话题id有误") Integer id) {
        return topicService.detail(id);
    }

    /**
     * 公共话题列表
     *
     * @param page {@link Page}
     * @return {@link Response}
     */
    @PostMapping("list")
    public Response list(@RequestBody @Validated Page page) {
        return topicService.list(page);
    }

    /**
     * 我创建的话题
     *
     * @param page {@link Page}
     * @return {@link Response}
     */
    @PostMapping("list/me")
    public Response listByCurrentUser(@RequestBody @Validated Page page) {
        return topicService.listMe(page);
    }

    /**
     * 创建新话题
     *
     * @param request {@link TopicCreateRequest}
     * @return {@link Response}
     */
    @PostMapping("new")
    @UserRoleIs(User.Role.COUNSELOR)
    public Response create(@RequestBody @Validated TopicCreateRequest request) {
        return topicService.create(request);
    }

    /**
     * 关闭话题
     *
     * @param request {@link TopicCloseRequest}
     * @return {@link Response}
     */
    @PostMapping("close")
    public Response close(@RequestBody @Validated TopicCloseRequest request) {
        return topicService.close(request);
    }
}
