package cn.org.processor.guestbook.api.util;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

public class ContextHolderUtil {
    public static HttpServletRequest mockHttpServletRequest = null;

    public static HttpServletRequest getRequest() {
        return Objects.requireNonNullElseGet(mockHttpServletRequest,
                () -> ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest());

    }

    public static Object getCurrentUser() {
        return getRequest().getAttribute("current_user");
    }

}
