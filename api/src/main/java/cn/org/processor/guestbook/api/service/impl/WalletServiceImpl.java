package cn.org.processor.guestbook.api.service.impl;

import cn.org.processor.guestbook.api.mapper.SettleMapper;
import cn.org.processor.guestbook.api.mapper.WalletMapper;
import cn.org.processor.guestbook.api.model.request.WalletRechargeRequest;
import cn.org.processor.guestbook.api.model.request.WalletSettleRequest;
import cn.org.processor.guestbook.api.service.WalletService;
import cn.org.processor.guestbook.api.util.ContextHolderUtil;
import entity.Settle;
import entity.User;
import entity.Wallet;
import io.Page;
import io.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import util.PageUtil;

import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class WalletServiceImpl implements WalletService {
    private final WalletMapper walletMapper;
    private final SettleMapper settleMapper;

    public WalletServiceImpl(WalletMapper walletMapper, SettleMapper settleMapper) {
        this.walletMapper = walletMapper;
        this.settleMapper = settleMapper;
    }

    @Override
    public Response getWallet() {
        User user = (User) ContextHolderUtil.getCurrentUser();
        Wallet wallet = walletMapper.findById(user.getId());
        if (wallet != null) {
            return Response.builder().ret(Response.Ret.SUCCESS).msg("获取成功").data(wallet).build();
        }

        log.error("Failed to fetch wallet for user: {}", user);
        return Response.builder().ret(Response.Ret.FAILURE).msg("获取失败").build();
    }

    @Override
    public Response recharge(WalletRechargeRequest request) {
        User user = (User) ContextHolderUtil.getCurrentUser();
        Wallet wallet = walletMapper.findById(user.getId());
        if (wallet == null) {
            log.error("Failed to fetch wallet for user: {}", user);
            return Response.builder().ret(Response.Ret.FAILURE).msg("服务器出错了").build();
        }

        wallet.setCoin(wallet.getCoin() + request.getCoin());
        int result = walletMapper.update(wallet);
        if (result > 0) {
            return Response.builder().ret(Response.Ret.SUCCESS).msg("充值成功").build();
        }

        log.error("Failed to recharge wallet for user: {}, recharge coin: {}", user, request.getCoin());
        return Response.builder().ret(Response.Ret.FAILURE).msg("充值失败").build();
    }

    @Override
    @Transactional
    public Response settle(WalletSettleRequest request) {
        User user = (User) ContextHolderUtil.getCurrentUser();
        Wallet wallet = walletMapper.findById(user.getId());
        if (wallet == null) {
            log.error("Failed to fetch wallet for user: {}", user);
            return Response.builder().ret(Response.Ret.FAILURE).msg("服务器出错了").build();
        }

        if (wallet.getCoin() < request.getCoin()) {
            return Response.builder().ret(Response.Ret.FAILURE).msg("余额不足").build();
        }

        wallet.setCoin(wallet.getCoin() - request.getCoin());
        int result = walletMapper.update(wallet);
        if (result > 0) {
            Settle settle = new Settle();
            settle.setUserId(user.getId());
            settle.setCoin(request.getCoin());
            settle.setMoney(request.getCoin() * 0.95);
            settle.setDate(new Date());
            result = settleMapper.create(settle);
            if (result > 0) {
                return Response.builder().ret(Response.Ret.SUCCESS).msg("提现成功").build();
            }

            log.error("Failed to insert settle for user: {}, settle coin: {}", user, request.getCoin());

            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Response.builder().ret(Response.Ret.FAILURE).msg("提现失败").build();
        }

        log.error("Failed to update wallet for user: {}, settle coin: {}", user, request.getCoin());
        return Response.builder().ret(Response.Ret.FAILURE).msg("提现失败").build();
    }

    @Override
    public Response settleLog(Page page) {
        User user = (User) ContextHolderUtil.getCurrentUser();
        List<Settle> list = settleMapper.listByUserId(user.getId(),
                PageUtil.getStartRows(page.getPage(), page.getOffset()), page.getOffset());

        return Response.builder().ret(Response.Ret.SUCCESS).msg("获取成功").data(list).build();
    }
}
