package cn.org.processor.guestbook.api.controller;

import annotation.UserStatusCheck;
import cn.org.processor.guestbook.api.model.request.ListMessageByTopicRequest;
import cn.org.processor.guestbook.api.model.request.MessageCreateRequest;
import cn.org.processor.guestbook.api.model.request.MessageDiscardRequest;
import cn.org.processor.guestbook.api.model.request.MessageReplyRequest;
import cn.org.processor.guestbook.api.service.MessageService;
import io.Page;
import io.Response;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;


@RestController
@RequestMapping("message")
@UserStatusCheck
public class MessageController {
    private final MessageService messageService;

    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    /**
     * 我的留言
     *
     * @param page {@link Page}
     * @return {@link Response}
     */
    @PostMapping("list/mine")
    public Response listByCurrentUser(@RequestBody @Validated Page page) {
        return messageService.listByCurrentUser(page);
    }

    /**
     * 列出我收到的留言
     *
     * @param page {@link Page}
     * @return {@link Response}
     */
    @PostMapping("list/receive")
    public Response listByReceive(@RequestBody @Validated Page page) {
        return messageService.listByReceive(page);
    }

    /**
     * 列出我捞到的留言
     *
     * @param page {@link Page}
     * @return {@link Response}
     */
    @PostMapping("list/pick")
    public Response listByPick(@RequestBody @Validated Page page) {
        return messageService.listByPick(page);
    }

    /**
     * 列出话题下的留言
     *
     * @param request {@link ListMessageByTopicRequest}
     * @return {@link Response}
     */
    @PostMapping("list/topic")
    public Response listByTopic(@RequestBody @Validated ListMessageByTopicRequest request) {
        return messageService.listByTopic(request);
    }
    /**
     * 拾取新留言
     *
     * @return {@link Response}
     */
    @GetMapping("pick")
    public Response pick() {
        return messageService.pick();
    }

    /**
     * 丢弃捞到的留言
     *
     * @param request {@link MessageDiscardRequest}
     * @return {@link Response}
     */
    @PostMapping("discard")
    public Response discard(@RequestBody @Validated MessageDiscardRequest request) {
        return messageService.discard(request);
    }

    /**
     * 创建新留言
     *
     * @param request {@link MessageCreateRequest}
     * @return {@link Response}
     */
    @PostMapping("new")
    public Response create(@RequestBody @Validated MessageCreateRequest request) {
        return messageService.create(request);
    }

    /**
     * 回复留言
     *
     * @param request {@link MessageReplyRequest}
     * @return {@link Response}
     */
    @PostMapping("reply")
    public Response reply(@RequestBody @Validated MessageReplyRequest request) {
        return messageService.reply(request);
    }

    /**
     * 查看留言详情
     *
     * @param id 留言id
     * @return {@link Response}
     */
    @GetMapping("{id}")
    public Response detail(@PathVariable("id")
                           @Validated
                           @NotNull(message = "参数错误")
                           @Positive(message = "留言id有误")
                           @Min(value = 1, message = "留言id有误") Integer id) {
        return messageService.detail(id);
    }
}
