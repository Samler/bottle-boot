package cn.org.processor.guestbook.api.service.impl;

import cn.org.processor.guestbook.api.mapper.UserProfileMapper;
import cn.org.processor.guestbook.api.model.request.ProfileUpdateRequest;
import cn.org.processor.guestbook.api.model.request.UpdateConsultPriceRequest;
import cn.org.processor.guestbook.api.service.UserProfileService;
import cn.org.processor.guestbook.api.util.ContextHolderUtil;
import entity.User;
import entity.UserProfile;
import io.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserProfileServiceImpl implements UserProfileService {
    private final UserProfileMapper userProfileMapper;

    public UserProfileServiceImpl(UserProfileMapper userProfileMapper) {
        this.userProfileMapper = userProfileMapper;
    }

    @Override
    public Response getProfile() {
        User user = (User) ContextHolderUtil.getCurrentUser();
        UserProfile profile = userProfileMapper.findById(user.getId());
        if (profile != null) {
            return Response.builder().ret(Response.Ret.SUCCESS).msg("获取成功").data(profile).build();
        }

        log.error("Not found the user profile by user id: {}", user.getId());
        return Response.builder().ret(Response.Ret.FAILURE).msg("获取失败").build();
    }

    @Override
    public Response getProfile(Integer id) {
        UserProfile profile = userProfileMapper.findById(id);
        if (profile != null) {
            // 去除个人隐私信息
            profile.setName(null);
            profile.setTelephone(null);
            profile.setMail(null);

            return Response.builder().ret(Response.Ret.SUCCESS).msg("获取成功").data(profile).build();
        }

        log.error("Not found the user profile for user id: {}", id);
        return Response.builder().ret(Response.Ret.FAILURE).msg("获取失败").build();
    }

    @Override
    public Response update(ProfileUpdateRequest request) {
        User user = (User) ContextHolderUtil.getCurrentUser();

        UserProfile profile = new UserProfile();
        profile.setId(user.getId());
        profile.setName(request.getName());
        profile.setTelephone(request.getTelephone());
        profile.setMail(request.getMail());
        profile.setIntro(request.getIntro());

        if (userProfileMapper.update(profile) > 0) {
            return Response.builder().ret(Response.Ret.SUCCESS).msg("已修改").build();
        }

        log.error("Failed to update the user profile for user id: {}", user.getId());
        return Response.builder().ret(Response.Ret.FAILURE).msg("修改失败").build();
    }

    @Override
    public Response setConsultPrice(UpdateConsultPriceRequest request) {
        User user = (User) ContextHolderUtil.getCurrentUser();

        UserProfile profile = new UserProfile();
        profile.setId(user.getId());
        profile.setConsultPrice(request.getPrice());

        if (userProfileMapper.updateConsultPrice(profile) > 0) {
            return Response.builder().ret(Response.Ret.SUCCESS).msg("已修改").build();
        }

        log.error("Failed to update the consult price for user id: {}", user.getId());
        return Response.builder().ret(Response.Ret.FAILURE).msg("修改失败").build();
    }
}
