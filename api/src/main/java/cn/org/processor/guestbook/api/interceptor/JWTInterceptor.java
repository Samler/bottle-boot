package cn.org.processor.guestbook.api.interceptor;


import annotation.IgnoreTokenCheck;
import cn.org.processor.guestbook.api.mapper.UserMapper;
import cn.org.processor.guestbook.api.util.InterceptorResponseUtil;
import cn.org.processor.guestbook.api.util.JWTUtil;
import cn.org.processor.guestbook.api.util.UserUtil;
import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.interfaces.DecodedJWT;
import entity.User;
import io.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.regex.Pattern;

/**
 * JWT拦截器
 */
@Component
@Slf4j
public class JWTInterceptor implements HandlerInterceptor {
    private final UserUtil userUtil;
    private final UserMapper userMapper;
    private final JWTUtil jwtUtil;

    JWTInterceptor(UserUtil userUtil, UserMapper userMapper, JWTUtil jwtUtil) {
        this.userUtil = userUtil;
        this.userMapper = userMapper;
        this.jwtUtil = jwtUtil;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        String token = request.getHeader("access_token");
        // 如果不是映射到方法直接通过
        if (!(handler instanceof HandlerMethod))
            return true;

        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();
        Class<?> clazz = handlerMethod.getBean().getClass();
        // 检查是否需要进行jwt拦截检测
        if (!method.isAnnotationPresent(IgnoreTokenCheck.class) &&
                !clazz.isAnnotationPresent(IgnoreTokenCheck.class)) {
            DecodedJWT jwtObj = (token != null && Pattern.matches("[\\w-]+\\.[\\w-]+\\.[\\w-]+", token)) ? jwtUtil.verify(token) : null;
            if (jwtObj != null) {
                JSONObject json = jwtUtil.getPayload(jwtObj);
                Integer uid = (Integer) (json.getOrDefault("uid", null));
                if (uid != null) {
                    User user = userMapper.findById(uid);

                    //验证是否修改过密码
                    if (user != null && jwtUtil.isAvailable(jwtObj, "token", UserUtil.getToken(user))) {
                        //如果需要重新创建一个token 则通知客户端保存新的toekn 并且将新的token返回
                        if (jwtUtil.isNeedRefresh(jwtObj)) {
                            Response resp = Response.builder()
                                    .ret(Response.Ret.TOKEN_REFRESHED)
                                    .msg("access token refreshed")
                                    .data(userUtil.getAccessToken(user))
                                    .build();

                            InterceptorResponseUtil.response(response, resp);
                            return false;
                        }

                        request.setAttribute("current_user", user);
                        return true;
                    }
                }
            }

            //验证未通过
            InterceptorResponseUtil.response(response, Response.builder()
                    .ret(Response.Ret.TOKEN_INVALID)
                    .msg("access token invalid")
                    .build());
            return false;
        }

        return true;
    }
}
