package cn.org.processor.guestbook.api.mapper;

import entity.UserProfile;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserProfileMapper {
    UserProfile findById(int id);

    int update(UserProfile profile);

    int createById(int id);

    int deleteById(int id);
    List<UserProfile> findByUserIds(List<Integer> userIds);

    int updateConsultPrice(UserProfile profile);
}
