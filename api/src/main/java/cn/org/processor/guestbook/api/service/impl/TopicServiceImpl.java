package cn.org.processor.guestbook.api.service.impl;

import cn.org.processor.guestbook.api.mapper.TopicMapper;
import cn.org.processor.guestbook.api.model.request.TopicCloseRequest;
import cn.org.processor.guestbook.api.model.request.TopicCreateRequest;
import cn.org.processor.guestbook.api.service.TopicService;
import cn.org.processor.guestbook.api.util.ContextHolderUtil;
import entity.Topic;
import entity.User;
import io.Page;
import io.Response;
import org.springframework.stereotype.Service;
import util.PageUtil;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class TopicServiceImpl implements TopicService {
    private final TopicMapper topicMapper;

    public TopicServiceImpl(TopicMapper topicMapper) {
        this.topicMapper = topicMapper;
    }

    @Override
    public Response list(Page page) {
        List<Topic> topics = topicMapper.list(PageUtil.getStartRows(page.getPage(), page.getOffset()), page.getOffset());
        topics.forEach(topic -> {
           if (topic.getContent().length() > 100) {
               topic.setContent(topic.getContent().substring(0, 100) + "...");
           }
        });

        return Response.builder().ret(Response.Ret.SUCCESS).msg("获取成功").data(topics).build();
    }

    @Override
    public Response listMe(Page page) {
        User user = (User) ContextHolderUtil.getCurrentUser();
        List<Topic> topics = topicMapper.listByUserId(user.getId(),
                PageUtil.getStartRows(page.getPage(), page.getOffset()), page.getOffset());
        topics.forEach(topic -> {
            if (topic.getContent().length() > 100) {
                topic.setContent(topic.getContent().substring(0, 100) + "...");
            }
        });

        return Response.builder().ret(Response.Ret.SUCCESS).msg("获取成功").data(topics).build();
    }

    @Override
    public Response create(TopicCreateRequest request) {
        User user = (User) ContextHolderUtil.getCurrentUser();

        Topic topic = new Topic();
        topic.setUserId(user.getId());
        topic.setTitle(request.getTitle());
        topic.setContent(request.getContent());
        topic.setDate(new Date());
        topic.setStatus(Topic.Status.OPEN);

        int result = topicMapper.create(topic);
        if (result > 0) {
            return Response.builder().ret(Response.Ret.SUCCESS).msg("创建成功").data(topic.getId()).build();
        }

        return Response.builder().ret(Response.Ret.FAILURE).msg("创建失败").build();
    }

    @Override
    public Response close(TopicCloseRequest request) {
        Topic topic = topicMapper.findById(request.getId());
        if (topic == null) {
            return Response.builder().ret(Response.Ret.FAILURE).msg("话题不存在").build();
        }

        User user = (User) ContextHolderUtil.getCurrentUser();
        if (!Objects.equals(topic.getUserId(), user.getId())) {
            return Response.builder().ret(Response.Ret.FAILURE).msg("非法操作").build();
        }

        topic.setStatus(Topic.Status.CLOSE);
        if (topicMapper.update(topic) > 0) {
            return Response.builder().ret(Response.Ret.SUCCESS).msg("关闭成功").build();
        }

        return Response.builder().ret(Response.Ret.FAILURE).msg("关闭失败").build();
    }

    @Override
    public Response detail(Integer id) {
        Topic topic = topicMapper.findById(id);
        if (topic == null) {
            return Response.builder().ret(Response.Ret.FAILURE).msg("话题不存在").build();
        }

        return Response.builder().ret(Response.Ret.SUCCESS).msg("获取成功").data(topic).build();
    }
}
