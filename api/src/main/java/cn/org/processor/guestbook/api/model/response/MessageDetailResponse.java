package cn.org.processor.guestbook.api.model.response;

import entity.Message;
import entity.Reply;
import lombok.Data;

import java.util.List;

@Data
public class MessageDetailResponse {
    private Message message;
    private List<Reply> replyList;
    private List<UserData> userData;

    @Data
    public static class UserData {
        private Integer id;
        private String username;
        private String intro;
    }
}
