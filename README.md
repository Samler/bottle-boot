# Guestbook Boot

[![pipeline status](https://git.samler.cn/Samler/guestbook-boot/badges/master/pipeline.svg)](https://git.samler.cn/Samler/guestbook-boot/-/commits/master)
[![coverage report](https://git.samler.cn/Samler/guestbook-boot/badges/master/coverage.svg)](https://git.samler.cn/Samler/guestbook-boot/-/commits/master)
[![Latest Release](https://git.samler.cn/Samler/guestbook-boot/-/badges/release.svg)](https://git.samler.cn/Samler/guestbook-boot/-/releases)

## 技术实现
Spring Boot、Mybatis

## 模块说明
`api` 用户API实现

`common` 公共模块

`admin` 管理员API实现

## 构建方法
在`Maven`中安装`common`模块后，即可构建出`api`和`admin`
```shell
mvn install -f common/pom.xml
```

打包`api`模块
```shell
mvn package -f api/pom.xml
```

打包`admin`模块
```shell
mvn package -f admin/pom.xml
```
